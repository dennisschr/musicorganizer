package nl.dennisschroer.musicorganizer.exception;

public class NotADirectoryException extends Exception {

    private String path;

    public NotADirectoryException(String path) {
	this.path = path;
    }

    public String getPath() {
	return path;
    }
    
    @Override
    public String getMessage() {
        return path + " is not a directory";
    }
}
