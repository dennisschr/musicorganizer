package nl.dennisschroer.musicorganizer.tags;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;

public class AlbumTag extends SongTag {

    public AlbumTag() {
	super("Artist");
    }

    @Override
    public String get(AudioFile file) {
	Tag tag = file.getTag();
	String album = null;
	if (tag != null) {
	    album = tag.getFirst(FieldKey.ALBUM);
	}
	return album;
    }

}
