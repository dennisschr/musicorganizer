package nl.dennisschroer.musicorganizer.tags;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagField;

public class ArtistTag extends SongTag {
	
	public ArtistTag() {
		super("Artist");
	}

	@Override
	public String get(AudioFile file) {
		Tag tag = file.getTag();
		String artist = null;
		if(tag!=null){
			artist = tag.getFirst(FieldKey.ALBUM_ARTIST);
			if(artist == null || artist.trim().equals("")) artist = tag.getFirst(FieldKey.ARTIST);
			if(artist == null || artist.trim().equals("")) artist = tag.getFirst(FieldKey.ARTISTS);
		}
		return artist;
	}

}
