package nl.dennisschroer.musicorganizer.tags;

import org.jaudiotagger.audio.AudioFile;

public abstract class SongTag {
	private String identifier;

	public SongTag(String identifier) {
		this.identifier = identifier;
	}

	public String getIdentifier() {
		return identifier;
	}

	protected abstract String get(AudioFile file);
	
	public String getValue(AudioFile file){
	    String value = this.get(file);
	    if(value.trim().equals("")) value = null;
	    return value;
	}
}
