package nl.dennisschroer.musicorganizer.tags;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagField;

public class TitleTag extends SongTag {

    public TitleTag() {
	super("Title");
    }

    @Override
    public String get(AudioFile file) {
	Tag tag = file.getTag();

	String title = null;

	if (tag != null) {
	    title = tag.getFirst(FieldKey.TITLE);
	}

	return title;
    }

}
