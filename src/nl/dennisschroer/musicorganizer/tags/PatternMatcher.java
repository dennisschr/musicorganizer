package nl.dennisschroer.musicorganizer.tags;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;

public class PatternMatcher {
	private final static String FORBIDDEN_CHARACTERS_REGEX = "[/\\\\:*?\"<>|]";
	private static final String FORBIDDEN_CHARACTER_REPLACEMENT = "-";
	
	private static Map<String, SongTag> elements = new HashMap<String, SongTag>();
	
	static{
		register(new ArtistTag());
		register(new ExtensionTag());
		register(new ArtistsTag());
	}
	
	public static void register(SongTag element){
		elements.put(element.getIdentifier(), element);
	}
	
	public static String match(AudioFile file, String pattern, String unknown) {
		Pattern regex = Pattern.compile("<(\\w+)>");
		Matcher matcher = regex.matcher(pattern);
		
		String output = new String(pattern);
		
		String key,value;
		
		while (matcher.find()) {
			key = matcher.group(1);
			value = null;
			
			if(elements.containsKey(key)){
				// Get it from the PatterElement
				value = elements.get(key).get(file);
			}else{
				// Try to get a tag with the same name
				Tag tag = file.getTag();
				if(tag!=null){
					FieldKey fieldKey = FieldKey.valueOf(key.toUpperCase());
					if (key!=null) value = tag.getFirst(fieldKey);
				}
			}
			
			// Set to unknown if not known or empty
			if(value==null || value.trim().equals("")){
				value = unknown;
			}
			
			// Remove forbidden characters
			value = handleForbiddenCharacters(value);
			
			// replace
			output = output.replace(matcher.group(0), value.trim());
	    }
		
		return output;
	}

	/**
	 * This method replaces forbidden filename characters from a value
	 * @param value
	 * @return
	 */
	private static String handleForbiddenCharacters(String value) {
		// Replace forbidden characters
		return value.replaceAll(FORBIDDEN_CHARACTERS_REGEX, FORBIDDEN_CHARACTER_REPLACEMENT);
	}
}
