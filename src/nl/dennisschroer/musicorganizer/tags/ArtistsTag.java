package nl.dennisschroer.musicorganizer.tags;

import java.util.ArrayList;
import java.util.List;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;

public class ArtistsTag extends SongTag {
	
	private static final String SEPERATOR = "; ";
	private static final String SPLIT_REGEX = ";|/|ft\\.?";

	public ArtistsTag() {
		super("Artists");
	}

	@Override
	public String get(AudioFile file) {
		Tag tag = file.getTag();
		List<String> artists = new ArrayList<String>();
		if(tag!=null){
			// Get main artist			
			String artist = tag.getFirst(FieldKey.ALBUM_ARTIST);
			if(artist == null || artist.trim().equals("")) artist = tag.getFirst(FieldKey.ARTIST);
			if(artist == null || artist.trim().equals("")) artist = tag.getFirst(FieldKey.ARTISTS);
		
			// Add it as first artist
			if(artist != null && !artist.trim().equals("")) artists.add(artist);
			
			for(String a : tag.getAll(FieldKey.ARTISTS)){
				// Split on ; or /
				String[] as = a.split(SPLIT_REGEX);
				for(String b : as){
					if(!artists.contains(b.trim())) artists.add(b.trim());
				}
			}
			for(String a : tag.getAll(FieldKey.ARTIST)){
				// Split on ; or /
				String[] as = a.split(SPLIT_REGEX);
				for(String b : as){
					if(!artists.contains(b.trim())) artists.add(b.trim());
				}
			}
		}
		
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i<artists.size(); i++){
			builder.append(artists.get(i));
			if(i!=artists.size()-1) builder.append(SEPERATOR);
		}
		
		
		return builder.length() > 0 ? builder.toString() : null;
	}

}
