package nl.dennisschroer.musicorganizer.tags;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagField;

public class ExtensionTag extends SongTag {

    public ExtensionTag() {
	super("Extension");
    }

    @Override
    public String get(AudioFile file) {
	String name = file.getFile().getName();
	int i = name.lastIndexOf('.');
	if (i > 0) {
	    return name.substring(i+1);
	}
	return null;
    }

}
