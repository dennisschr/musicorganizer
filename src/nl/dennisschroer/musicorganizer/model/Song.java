package nl.dennisschroer.musicorganizer.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import org.jaudiotagger.audio.AudioFile;

public class Song {
    private AudioFile audioFile;

    private final StringProperty pathProperty;
    private final StringProperty artistProperty;
    private final StringProperty artistsProperty;
    private final StringProperty titleProperty;
    private final StringProperty albumProperty;
    private final StringProperty extensionProperty;
    

    public Song(AudioFile audioFile) {
	this.audioFile = audioFile;
	this.pathProperty = new SimpleStringProperty(audioFile.getFile().getPath());
	this.artistProperty = new SimpleStringProperty(this.getSongTag(SongTags.ARTIST));
	this.artistsProperty = new SimpleStringProperty(this.getSongTag(SongTags.ARTISTS));
	this.titleProperty = new SimpleStringProperty(this.getSongTag(SongTags.TITLE));
	this.albumProperty = new SimpleStringProperty(this.getSongTag(SongTags.ALBUM));
	this.extensionProperty = new SimpleStringProperty(this.getSongTag(SongTags.EXTENSION));
    }
    
    private String getSongTag(SongTags songTags) {
	return songTags.getSongTag().getValue(this.audioFile);
    }
    
    public StringProperty albumProperty() {
	return albumProperty;
    }

    public StringProperty pathProperty(){
	return this.pathProperty;
    }
    
    public StringProperty artistProperty(){
	return this.artistProperty;
    }
    
    public StringProperty artistsProperty(){
	return this.artistsProperty;
    }
    
    public StringProperty titleProperty(){
	return this.titleProperty;
    }
    
    public StringProperty extensionProperty() {
	return this.extensionProperty;
    }
}
