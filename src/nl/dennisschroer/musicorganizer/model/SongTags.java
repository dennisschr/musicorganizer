package nl.dennisschroer.musicorganizer.model;

import nl.dennisschroer.musicorganizer.tags.AlbumTag;
import nl.dennisschroer.musicorganizer.tags.ArtistTag;
import nl.dennisschroer.musicorganizer.tags.ArtistsTag;
import nl.dennisschroer.musicorganizer.tags.ExtensionTag;
import nl.dennisschroer.musicorganizer.tags.SongTag;
import nl.dennisschroer.musicorganizer.tags.TitleTag;

public enum SongTags {
    ALBUM(new AlbumTag()), ARTIST(new ArtistTag()), ARTISTS(new ArtistsTag()), EXTENSION(new ExtensionTag()), TITLE(new TitleTag());
    
    private SongTag songTag;

    SongTags(SongTag songTag){
	this.songTag = songTag;
    }
    
    public SongTag getSongTag() {
	return songTag;
    }
}
