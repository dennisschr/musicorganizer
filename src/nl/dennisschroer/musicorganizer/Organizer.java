package nl.dennisschroer.musicorganizer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Scanner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import nl.dennisschroer.musicorganizer.config.ConfigValues;
import nl.dennisschroer.musicorganizer.exception.NotADirectoryException;
import nl.dennisschroer.musicorganizer.model.Song;
import nl.dennisschroer.musicorganizer.tags.PatternMatcher;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.TagField;

public class Organizer {
    static Logger log = LogManager.getLogger(Organizer.class);

    private static final String DELIMITER = System
	    .getProperty("file.seperator") != null ? System
	    .getProperty("file.seperator") : "/";

    private static final String ACTION_MOVE = "move";
    private static final String ACTION_COPY = "copy";

    private final static String FORBIDDEN_DIRECTORY_START_REGEX = "[\\\\/]\\.+";

    private static final boolean DEBUG_PRINT_TAGS = false;

    private static String CONFIG_PATH = "./MusicOrganizer.properties";

    private File sourceFolder;
    private File targetFolder;
    private String pattern;
    private String unknown;
    private String action;
    private boolean removeEmptyDirectories;

    /**
     * Mapping containing the moves to be made, as the original location to the
     * target location
     */
    private Map<String, String> moves;

    private Configuration config;

    private List<Path> sources;

    private SongFileVisitor visitor;

    private ObservableList<Song> sourceSongs;

    public Organizer(Configuration config) {
	this.config = config;
	this.sourceSongs = FXCollections.observableArrayList();

	this.init();

	/*
	 * config.getStr String source = config.getProperty("source", "./NEW");
	 * String target = config.getProperty("target", "./TARGET");
	 * 
	 * this.pattern = config.getProperty("pattern",
	 * "<Artist>/<Album>/<Artist> - <Title>.<Extension>"); this.unknown =
	 * config.getProperty("unknown", "Onbekend"); this.action =
	 * config.getProperty("action", ACTION_MOVE);
	 * this.removeEmptyDirectories =
	 * config.getProperty("removeemptydirectories").equals("1");
	 * 
	 * this.sourceFolder = new File(source); this.targetFolder = new
	 * File(target);
	 */
    }

    public void init() {
	String[] sourcePaths = config.getStringArray(ConfigValues.SOURCES
		.toString());

	log.debug(sourcePaths);

	try {
	    this.handleSources(sourcePaths);
	} catch (NotADirectoryException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	this.pattern = config.getString(ConfigValues.PATTERN.toString(),
		"<Artist>/<Album>/<Artist> - <Title>.<Extension>");
	this.unknown = config.getString(ConfigValues.UNKNOWN.toString(),
		"Unknown");

	// this.scanSources();
    }

    public void handleSources(String[] paths) throws NotADirectoryException {
	sources = new ArrayList<Path>();

	for (String path : paths) {
	    Path p = Paths.get(path);
	    if (p.toFile().isDirectory()) {
		this.sources.add(p);
	    } else {
		throw new NotADirectoryException(path);
	    }
	}
    }

    public void scanSources() {
	if (this.visitor == null) {
	    this.visitor = new SongFileVisitor(this.sourceSongs);
	}

	for (Path path : this.sources) {
	    try {
		log.debug("Visiting " + path.toString());
		Files.walkFileTree(path, visitor);
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

    public ObservableList<Song> getSongs() {
	return this.sourceSongs;
    }

    private void run() {
	// Check directories
	if (!preChecks())
	    return;

	// Initialize mapping
	moves = new HashMap<String, String>();

	// Scan sourceFolder
	System.out.println("Checking the source folder for music files...");
	handleSourceFolder(sourceFolder);

	// Remember how many files need to be moved
	int collisions = 0;
	int moving = 0;

	// Print files which have to be moved
	for (Entry<String, String> move : moves.entrySet()) {
	    if (move.getKey().equals(move.getValue())) {
		// System.out.println("Same: " + move.getKey());
	    } else {
		System.out.println("From: " + move.getKey());
		System.out.println("  To: " + move.getValue());
		moving++;
	    }
	}

	// Check if some files are mapped to the same destination
	Map<String, String> targets = new HashMap<String, String>(moves.size());
	for (Entry<String, String> move : moves.entrySet()) {
	    if (targets.containsKey(move.getValue())) {
		System.err.println("Two files mapped to the same destination:");
		System.err.println("  1. " + targets.get(move.getValue()));
		System.err.println("  2. " + move.getKey());
		System.err.println("  Destination: " + move.getValue());
		collisions++;
	    } else {
		targets.put(move.getValue(), move.getKey());
	    }
	}

	// Generate summary
	System.out.println("============= RESULT =============");
	System.out.println("  Files detected: " + moves.size());
	System.out.println("  Files to move: " + moving);
	System.out.println("  Collisions: " + collisions);
	System.out.println("  Action: " + action);
	System.out.println("  Remove empty directories: "
		+ (removeEmptyDirectories ? "Yes" : "No"));
	System.out.println("=================================");

	if (collisions > 0) {
	    System.out
		    .println("Sorry, you have to fix the collission first! Done :|");
	} else if (moving == 0) {
	    System.out.println("Thats easy, I have nothing to do! Done :D");
	} else {
	    // Ask confirmation
	    System.out.print("Continue? (y/n): ");
	    Scanner scanner = new Scanner(System.in);
	    while (!scanner.hasNext()) {
	    }
	    String input = scanner.next();

	    if (input.equals("y")) {
		if (action.equals(ACTION_MOVE)) {
		    // Move all files
		    FileUtils.moveAll(moves);

		} else {
		    // Copy all files
		    FileUtils.copyAll(moves);
		}

		System.out.println("Moving/Copying completed");

		if (this.removeEmptyDirectories) {
		    System.out.println("Removing empty directories...");
		    FileUtils.removeEmptyDirectories(sourceFolder);
		}

		System.out.println("Done :)");
	    } else {
		System.out.println("Ok�, dan niet :(! Stopping...");
	    }
	    scanner.close();
	}
    }

    private boolean preChecks() {
	boolean result = true;

	// Check if action is correct
	if (!action.equals(ACTION_MOVE) && !action.equals(ACTION_COPY)) {
	    System.err.println("Action '" + action + "' is invalid");
	    result = false;
	}

	// Check if the paths are directories
	if (!sourceFolder.isDirectory()) {
	    System.err.println("Source is not a directory ("
		    + sourceFolder.getPath() + ")");
	    result = false;
	}

	if (!targetFolder.isDirectory()) {
	    System.err.println("Target is not a directory ("
		    + targetFolder.getPath() + ")");
	    result = false;
	}

	return result;
    }

    private void handleSourceFolder(File sourceFolder) {
	for (File file : sourceFolder.listFiles()) {
	    if (file.isDirectory()) {
		this.handleSourceFolder(file);
	    } else {
		this.handleSourceFile(file);
	    }
	}
    }

    /**
     * Handle the given file and put it in <i>moves</i> if it is a music file
     * 
     * @param file
     */
    private void handleSourceFile(File file) {
	try {
	    // Read the file
	    AudioFile f = AudioFileIO.read(file);

	    if (DEBUG_PRINT_TAGS)
		this.printTags(f);

	    // Get the source path
	    String from = file.getPath();

	    // Extract data from the file and put it in the pattern to create
	    // the destination path
	    String to = targetFolder.getPath() + DELIMITER
		    + PatternMatcher.match(f, pattern, unknown);

	    // Do some normalizations
	    to = normalizePath(to);
	    from = normalizePath(from);

	    // Put it in the mapping
	    moves.put(from, to);
	} catch (CannotReadException e) {
	    // Not a music file
	    // TODO Auto-generated catch block
	    // e.printStackTrace();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (TagException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (ReadOnlyFileException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (InvalidAudioFrameException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    /**
     * Print all found tags for the given AudioFile
     * 
     * @param file
     */
    private void printTags(AudioFile file) {
	Tag tag = file.getTag();
	if (tag != null) {
	    Iterator<TagField> it = tag.getFields();
	    while (it.hasNext()) {
		TagField field = it.next();
		System.out.println(field.getId() + ": " + field.toString());
	    }
	} else {
	    System.out.println("No Tag");
	}
    }

    /**
     * Transform the path so it only contains the system delimiter
     * 
     * @param path
     *            The path to transform
     * @return The resulting path
     */
    private String normalizePath(String path) {
	String temp = path.replaceAll("[\\\\/]", DELIMITER);
	// Directories starting with a dot are not allowed
	return temp.replaceAll(FORBIDDEN_DIRECTORY_START_REGEX, DELIMITER);
    }

    public static void main(String[] args) {
	File configFile = new File(CONFIG_PATH);

	System.out
		.println("Thanks for using this amazing simple music organizer :)");

	try {
	    if (!configFile.exists()) {
		System.out.println("Creating new config file ...");
		System.out.println("  path: " + configFile.getPath());
		Organizer.createBasicConfig();
		System.out
			.println("  File created, please edit before running again");
	    } else {
		// Load configuration
		System.out.println("Loading configuration ... ");
		System.out.println("  path: " + configFile.getPath());
		Properties config = new Properties();
		config.load(new FileInputStream(configFile));

		// Start the organizer
		// Organizer mo = new Organizer(config);
		// mo.run();
	    }
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    private static void createBasicConfig() throws IOException {
	File file = new File(CONFIG_PATH);
	file.createNewFile();

	BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
		new FileOutputStream(file)));

	writer.write("# Directory (including subdirectories) where to look for files to organize");
	writer.newLine();
	writer.write("source=./new");
	writer.newLine();

	writer.write("# Directory where to put the organized files");
	writer.newLine();
	writer.write("target=./target");
	writer.newLine();

	writer.write("# Name pattern of the relative path to put the file");
	writer.newLine();
	writer.write("pattern=<Artist>/<Album>/<Artist> - <Title>.<Extension>");
	writer.newLine();

	writer.write("# Replacement when some information is unknown");
	writer.newLine();
	writer.write("unknown=Onbekend");
	writer.newLine();

	writer.write("# What to do with the files (copy or move)?");
	writer.newLine();
	writer.write("action=move");
	writer.newLine();

	writer.write("# Remove empty directories after running (1 or 0)?");
	writer.newLine();
	writer.write("removeemptydirectories=1");
	writer.newLine();

	writer.flush();

	writer.close();
    }
}
