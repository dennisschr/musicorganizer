package nl.dennisschroer.musicorganizer.javafx;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import nl.dennisschroer.musicorganizer.javafx.lang.Language;
import nl.dennisschroer.musicorganizer.javafx.lang.LanguageLines;
import nl.dennisschroer.musicorganizer.model.Song;

public class MainStage extends Stage {

    private BorderPane root;
    private TableView<Song> songsListView;
    private HBox topMenuView;
    private MainController controller;
    private static List<ColumnDescription> availableColumns = new ArrayList<ColumnDescription>();
    
    static{
	availableColumns.add(new ColumnDescription<String>("path", LanguageLines.PATH));
	availableColumns.add(new ColumnDescription<String>("artist", LanguageLines.ARTIST));
	availableColumns.add(new ColumnDescription<String>("artists", LanguageLines.ARTISTS));
	availableColumns.add(new ColumnDescription<String>("title", LanguageLines.TITLE));
	availableColumns.add(new ColumnDescription<String>("album", LanguageLines.ALBUM));
	availableColumns.add(new ColumnDescription<String>("extension", LanguageLines.EXTENSION));
    }

    public MainStage(MainController controller){
	super();
	this.controller = controller;
    }
    
    /** Build this stage */
    public void build(){
	root = new BorderPane();
	
	buildSongListView();
	buildTopMenu();
	
	root.setTop(topMenuView);
	root.setCenter(songsListView);
	
	this.setTitle("Music Organizer");
	
	Scene scene = new Scene(root, 800, 600);
	scene.getStylesheets().add("/style/main.css");
	
	this.setScene(scene);
    }

    private void buildTopMenu() {
	topMenuView = new HBox();
	
	Button refreshButton = new Button(Language.getString(LanguageLines.BUTTON_REFRESH));
	refreshButton.setId(MainController.BUTTON_REFRESH);
	refreshButton.setOnAction(this.controller);
	
	topMenuView.getChildren().add(refreshButton);	
    }

    private void buildSongListView() {
	songsListView = new TableView<Song>(this.controller.getApplication().getOrganizer().getSongs());
	
	// For now: add all available columns	
	for(ColumnDescription columnDescription : availableColumns){
	    songsListView.getColumns().add(columnDescription.createColumn());
	}
	
	songsListView.getStyleClass().add("songs-list");
    }
    
}
