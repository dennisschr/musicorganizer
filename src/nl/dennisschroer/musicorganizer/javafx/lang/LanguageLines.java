package nl.dennisschroer.musicorganizer.javafx.lang;

public enum LanguageLines {
    ALBUM, ARTIST, ARTISTS, BUTTON_REFRESH, EXTENSION, PATH, TITLE

}
