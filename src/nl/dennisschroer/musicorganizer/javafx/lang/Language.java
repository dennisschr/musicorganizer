package nl.dennisschroer.musicorganizer.javafx.lang;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class Language {

    private static Language instance;
    private Configuration config;

    private Language(Configuration config) {
	this.config = config;
    }
    
    public Configuration getConfig() {
	return config;
    }

    public static String getString(LanguageLines line) {
	return getString(line.toString());
	
    }

    public static String getString(String line) {
	return instance.getConfig().getString(line, line);    }

    /**
     * Load a new instance of Language and return it
     * @param language
     * @return
     * @throws ConfigurationException
     */
    public static Language load(String language) throws ConfigurationException {
	Configuration languageConfig = new PropertiesConfiguration("lang/" + language + ".properties");
	instance = new Language(languageConfig);
	return instance;
    }

}
