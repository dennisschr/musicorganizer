package nl.dennisschroer.musicorganizer.javafx;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import nl.dennisschroer.musicorganizer.javafx.lang.Language;
import nl.dennisschroer.musicorganizer.javafx.lang.LanguageLines;
import nl.dennisschroer.musicorganizer.model.Song;

public class ColumnDescription<T> {

    private String propertyName;
    private LanguageLines languageLine;

    public ColumnDescription(String propertyName, LanguageLines languageLine) {
	this.propertyName = propertyName;
	this.languageLine = languageLine;
    }
    
    public LanguageLines getLanguageLine() {
	return languageLine;
    }
    
    public String getPropertyName() {
	return propertyName;
    }

    public TableColumn<Song, T> createColumn() {
	TableColumn<Song, T> column = new TableColumn<Song, T>(Language.getString(this.getLanguageLine()));
	column.setCellValueFactory(new PropertyValueFactory<Song, T>(this.getPropertyName()));
	return column;
    }

}
