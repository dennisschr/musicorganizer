package nl.dennisschroer.musicorganizer.javafx;

import nl.dennisschroer.musicorganizer.task.RefreshTask;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class MainController implements EventHandler<ActionEvent> {
    
    public static final String BUTTON_REFRESH = "button-refresh";
    
    private MusicOrganizerApplication application;

    public MainController(MusicOrganizerApplication application) {
	this.application = application;
    }
    
    public MusicOrganizerApplication getApplication() {
	return application;
    }

    @Override
    public void handle(ActionEvent event) {
	if(event.getSource() instanceof Button){
	    Button button = (Button) event.getSource();
	    switch(button.getId()){
	    	case BUTTON_REFRESH:
	    	    application.getTaskManager().add(new RefreshTask(application));
	    }
	}
	
    }

}
