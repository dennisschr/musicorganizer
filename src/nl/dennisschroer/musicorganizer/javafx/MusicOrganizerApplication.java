package nl.dennisschroer.musicorganizer.javafx;

import javafx.application.Application;
import javafx.stage.Stage;
import nl.dennisschroer.musicorganizer.Organizer;
import nl.dennisschroer.musicorganizer.javafx.lang.Language;
import nl.dennisschroer.musicorganizer.task.TaskManager;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class MusicOrganizerApplication extends Application {
    
    static Logger log = LogManager.getLogger(MusicOrganizerApplication.class);
    
    private static String CONFIG_PATH = "MusicOrganizer.properties";

    private Organizer organizer;

    private Configuration config;

    private Language language;

    private TaskManager taskManager;

    @Override
    public void start(Stage primaryStage) throws Exception {	
	// Load configuration
	this.config = new PropertiesConfiguration(CONFIG_PATH);
	// Determine language
	this.language = Language.load(config.getString("language", "english"));
	
	this.organizer = new Organizer(this.config);
	this.taskManager = new TaskManager();
	
	MainStage stage = new MainStage(new MainController(this));
	stage.build();
	stage.show();
    }
    
    public Organizer getOrganizer() {
	return organizer;
    }

    public static void main(String[] args) {
	launch(args);
    }

    public Language getLanguage() {
	return this.language;
    }

    public TaskManager getTaskManager() {
	return this.taskManager;
    }
}
