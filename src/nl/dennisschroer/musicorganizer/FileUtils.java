package nl.dennisschroer.musicorganizer;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Map.Entry;

public class FileUtils {
	private static void executeCopy(String from, String to) throws IOException{
		Path fromPath = FileSystems.getDefault().getPath(from);
		Path toPath = FileSystems.getDefault().getPath(to);
		Files.createDirectories(toPath.getParent());
		Files.copy(fromPath, toPath);
	}
	
	private static void executeMove(String from, String to) throws IOException{
		Path fromPath = FileSystems.getDefault().getPath(from);
		Path toPath = FileSystems.getDefault().getPath(to);
		Files.createDirectories(toPath.getParent());
		Files.move(fromPath, toPath);
	}

	public static void moveAll(Map<String, String> moves) {
		for (Entry<String, String> move : moves.entrySet()) {
			if(move.getKey().equals(move.getValue())) continue;
			try {
				executeMove(move.getKey(), move.getValue());
				System.out.println("  Moved: " + move.getKey());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void copyAll(Map<String, String> moves) {
		for (Entry<String, String> move : moves.entrySet()) {
			if(move.getKey().equals(move.getValue())) continue;
			try {
				executeCopy(move.getKey(), move.getValue());
				System.out.println("  Copied: " + move.getKey());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Remove empty directories in the source folder
	 * @return Whether this directory is empty after removing empty directories
	 */
	public static boolean removeEmptyDirectories(File sourceFolder) {
		boolean empty = true;
		for(File file : sourceFolder.listFiles()){
			if(file.isDirectory()){
				if(removeEmptyDirectories(file)){
					// This subdirectory is empty after removing empty subsubdirectories
					// Remove this directory
					System.out.println("  Removing directory " + file.getPath());
					if(!file.delete()){
						System.err.println("Unable to delete " + file.getPath());
						empty = false;
					}
				}else{
					empty = false;
				}
			}else{
				empty = false;
			}
		}
		return empty;
	}
}
