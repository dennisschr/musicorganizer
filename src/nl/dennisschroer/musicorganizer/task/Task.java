package nl.dennisschroer.musicorganizer.task;

public interface Task extends Runnable{
    public void cancel();
}
