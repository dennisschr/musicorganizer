package nl.dennisschroer.musicorganizer.task;

import nl.dennisschroer.musicorganizer.javafx.MusicOrganizerApplication;

public class RefreshTask implements Task {
    
    private MusicOrganizerApplication application;

    public RefreshTask(MusicOrganizerApplication application) {
	this.application = application;
    }

    @Override
    public void run() {
	this.application.getOrganizer().getSongs().clear();
	this.application.getOrganizer().scanSources();	
    }

    @Override
    public void cancel() {
	
    }
}
