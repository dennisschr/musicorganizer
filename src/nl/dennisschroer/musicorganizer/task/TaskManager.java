package nl.dennisschroer.musicorganizer.task;

import java.util.ArrayList;
import java.util.List;

/**
 * The TaskManager is responsible for the management of various tasks (which run in seperate threads in background)
 * @author dennis
 *
 */
public class TaskManager {
    private List<Thread> tasks;
    
    public TaskManager() {
	this.tasks = new ArrayList<Thread>();
    }

    public void add(Task task){
	Thread thread = new Thread(task);
	this.tasks.add(thread);
	thread.start();	
    }
}
