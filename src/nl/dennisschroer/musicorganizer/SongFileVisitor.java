package nl.dennisschroer.musicorganizer;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import nl.dennisschroer.musicorganizer.model.Song;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;

public class SongFileVisitor implements FileVisitor<Path> {
    
    static Logger log = LogManager.getLogger(SongFileVisitor.class);
    
    private List<Song> songs;

    /**
     * Create a new visitor which adds all songs it finds in the visitation to the given list (if it does not contain the song already)
     * @param songs The list to add the songs to
     */
    public SongFileVisitor(List<Song> songs) {
	this.songs = songs;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc)
	    throws IOException {
	return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
	    throws IOException {
	return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
	    throws IOException {
	try {
	    AudioFile f = AudioFileIO.read(file.toFile());
	    Song song = new Song(f);
	    if(!this.songs.contains(song)){
		this.songs.add(song);
	    }
	    log.debug("Song found " + file.toString());
	} catch (CannotReadException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (TagException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (ReadOnlyFileException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (InvalidAudioFrameException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc)
	    throws IOException {
	return FileVisitResult.CONTINUE;
    }
    
    public List<Song> getSongs() {
	return songs;
    }

}
