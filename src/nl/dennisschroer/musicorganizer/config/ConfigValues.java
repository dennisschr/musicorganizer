package nl.dennisschroer.musicorganizer.config;

public enum ConfigValues {
    SOURCES, PATTERN, UNKNOWN;
    
    public String toString(){
	return this.name().toLowerCase();
    }
}
